#!/usr/bin/python
#
# telepathy-butterfly - an MSN connection manager for Telepathy
#
# Copyright (C) 2006-2007 Ali Sabil <ali.sabil@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import gobject
import dbus.glib
import signal
import os
import sys

import logging

logging.basicConfig(level=logging.DEBUG)

from butterfly import ButterflyConnectionManager
from butterfly.util.decorator import async

logger = logging.getLogger('Butterfly')

IDLE_TIMEOUT = 5000
PROCESS_NAME = 'telepathy-butterfly'

def debug_divert_messages(filename):
    """debug_divert_messages:
    @filename: A file to which to divert stdout and stderr or None to do
    nothing.

    Open the given file for writing and duplicate its file descriptor to
    be used for stdout and stderr. This has the effect of closing the previous
    stdout and stderr, and sending all messages that would have gone there
    to the given file instead.

    By default the file is truncated and hence overwritten each time the
    process is executed.
    If the filename is prefixed with '+' then the file is not truncated and
    output is added at the end of the file.
    Passing None to this function is guaranteed to have no effect. This is
    so you can call it with the recommended usage
    debug_divert_messages (os.getenv(MYAPP_LOGFILE))
    and it won't do anything if the environment variable is not set.  """

    # TODO: this function should move to telepathy-python at some point

    if filename is None:
        print "caca"
        return

    try:
        if filename.startswith('+'):
            logfile = open(filename[1:], 'a')
        else:
            logfile = open(filename, 'w')
    except IOError, e:
        print "Can't open logfile '%s' : '%s'" %(filename, e)
        return

    sys.stdout = logfile
    sys.stderr = logfile

if __name__ == '__main__':
    debug_divert_messages(os.getenv('BUTTERFLY_LOGFILE'))

    try: # change process name for killall
       import ctypes
       libc = ctypes.CDLL('libc.so.6')
       libc.prctl(15, PROCESS_NAME, 0, 0, 0)
    except Exception, e:
       logger.warning('Unable to set processName: %s" % e')

    @async
    def quit():
        manager.quit()
        mainloop.quit()

    if 'BUTTERFLY_PERSIST' not in os.environ:
        def timeout_cb():
            if len(manager._connections) == 0:
                logger.info('No connection received - quitting')
                quit()
            return False
        gobject.timeout_add(IDLE_TIMEOUT, timeout_cb)
        shutdown_callback = quit
    else:
        shutdown_callback = None

    signal.signal(signal.SIGTERM, lambda : quit)

    manager = ButterflyConnectionManager(shutdown_callback)
    mainloop = gobject.MainLoop(is_running=True)

    while mainloop.is_running():
        try:
            mainloop.run()
        except KeyboardInterrupt:
            quit()
